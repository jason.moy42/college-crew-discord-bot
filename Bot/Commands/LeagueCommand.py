import discord

from envs import RIOT_API_KEY, LOGGER
from Bot.Commands.Command import Command


class LeagueCommand(Command):
    """
    Command to access the functions related to League of Legends.
    """
    ID = 'league'  # type: str

    def __init__(self):
        super().__init__()

    async def send_message(self, channel: discord.ChannelType, message: str, *args):
        pass

    async def execute(self, message: discord.Message):
        pass
